# Deployment of metainfo Services

Follwoing figure discussed the architecture of the metainfo platfrom. 

![Alt text](metainfo-architecture.png?raw=true "metainfo platfrom architecture")

## Services

There are three main servies;

```
1. elassandra
2. aplos
3. gateway
```


## Deploy services

Start services in following order;

```
docker-compose up -d elassandra
docker-compose up -d aplos
docker-compose up -d gateway
docker-compose up -d web
```
** After hosting website, it can be reached on `<Ip address of the Vm>:4410`.
** Open `7774` and `4410` port on VM for public

#### 1. put Card Managment

```
# request
curl -XPOST "http://localhost:7654/api/Card_Managment" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "1121xxt",
  "Card": "C1",
  "NameonCard": "majintha",
  "Type": "Type",
  "Date": "2021-10-12",
  "CVV": "CVV",
  "phone": "phone",
}
'

# reply
{"code":201,"msg":"Card added"}
```


#### 2. get Card 

```
# request
curl -XPOST "http://localhost:7654/api/Card_Managment" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "1121xxt",
  "Card": "C1",
  "NameonCard": "majintha",
  "Type": "Visa",
  "CVV": "001",
  "Date": "2021-10-12",
  "phone": "0766525850",
}
'

# reply
{"Card":"C1","NameonCard":"majintha","Type": "Visa","Date": "2021-11-04","CVV": "001", "phone": "0766525850" "timestamp":"2021-11-04 20:55:21.522"}
```
